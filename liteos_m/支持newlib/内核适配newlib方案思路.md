# LiteOS-M内核支持musl与newlib平滑切换方案

## 背景

当前M核编译时上层组件与内核统一使用musl-C，位于`//third_party/musl/porting/liteos_m/kernel/`，内核当前的内部实现也是主要适配musl的结构体定义，但社区及三方厂商开发多使用公版工具链`arm-none-eabi-gcc`加上私有定制优化进行编译，考虑我们内核的易用性提升，故支持公版`arm-none-eabi-gcc`C库编译内核运行。

## Newlib

1. newlib是小型C库，针对posix接口涉及系统调用的部分，newlib提供一些需要系统适配的钩子函数，例如`_exit()`，`_open()`，`_close()`，`_gettimeofday()`等，操作系统适配这些钩子，就可以使用公版newlib工具链编译运行程序。

2. 公版`arm-none-eabi-`工具链中带有通用newlib的C库头文件，在一份通用的C库头文件中，使用不同的宏开关来控制一些 宏、类型、结构体、函数的声明或者定义。这些宏有以下几种：

   1> 根据posix标准划分，例如：`_POSIX_TIMERS`,`_POSIX_CPUTIME`,`_POSIX_THREADS`等。

   2> 根据已支持的系统，例如：`__CYGWIN__`，`__rtems__`，`__unix__`等。

   3> 根据C库不同版本，例如：`_GNU_SOURCE`,`_BSD_SOURCE `等。

## 方案

### 当前内核现状

1. LiteOS-M不区分用户态内核态，上层组件与内核共用一套基于musl的C库，虽然编译时使用`arm-none-eabi-gcc`，但只使用其工具链的编译功能，通过加上`-nostdinc`与`-nostdlib`强制使用我们自己改造后的musl-C。

2. 内核提供了基于LOS_XXX适配实现pthread、mqeue、fs、semaphore、time等模块的posix接口（`//kernel/liteos_m/kal/posix`）
3. 内核提供的posix接口与musl中的标准C库接口共同组成LiteOS-M的LibC。

### 适配变更

1. 适配newlib预留的钩子
2. 针对当前内核的能力，去定义_POSIX_XXX的能力开关，来打开工具链里的所需内容的声明或定义
3. 针对当前内核的具体实现，如果newlib中结构体的定义或者宏与内核相冲突，以当前内核具体实现为准
4. 用自研的posix接口去补充或替换newlib的部分实现，根据posix标准补充newlib拓展头文件
5. 系统编译过程支持newlib与musl平滑切换

### 最终实现

LiteOS-M的LibC组成结构：

![](内核适配newlib方案思路.assets/内核LIBC组成结构.jpg)

newlib的适配实现：

1. 头文件的适配：

   | 原因类型               | 实施方法                                                     | 例如                              |
   | ---------------------- | ------------------------------------------------------------ | --------------------------------- |
   | 头文件缺失             | 根据系统的实现所需，补充相关的标准声明定义                   | <poll.h>                          |
   | 被特定宏控制的声明定义 | `//kernel/liteos_m/kal/libc/newlib/porting/include`中添加同名头文件，采用<br>`-I {kernel/liteos_m/kal/libc/newlib/porting/include}`加`#include_next <XXX.h>`的方式，编译时先找到我们新增的同名头文件，打开相关的宏开关，再找到工具链的原始头文件，获得所需的声明定义。 | <sys/features.h><br>+<br><time.h> |
   | 声明、定义差异         | 根据系统的具体实现，与`#include_next`结合使用，替换工具链中的原始声明定义 | <sys/_pthreadtypes.h>             |

2. 实现的适配：

   | 原因类型             | 实施方法                                        | 例如                                      |
   | -------------------- | ----------------------------------------------- | ----------------------------------------- |
   | 接口实现缺失         | 采用内核自研接口补充                            | kal/libc/newlib/porting/src/fs.c          |
   | newlib钩子适配       | 采用当前内核实现的LOS_XXX接口适配或者默认空实现 | kal/libc/newlib/porting/src/other_adapt.c |
   | 采用定制化的接口实现 | 通过wrap编译选项，替换工具链中的默认实现        | kal/libc/newlib/porting/src/malloc.c      |

### 使用方式

1. 当前产品编译时指定依赖内核后，会默认添加依赖内核的对外接口(LOS_XXX、libc)，不必显式指明include路径。
2. 在//kernel/liteos_m目录下使用`make menuconfig`-->`Compat`-->`Choose libc implementation`选择`newlibc`
3. 项目根目录`hb build -f`即可

## 注意

1. malloc函数相关功能当前采用了wrap方式，以采用内核提供的内存分配管理算法。
2. printf函数采用newlib默认实现，单板进行适配时，需要根据板端所需能力提供printf的实现。
3. 若newlib中缺少所需声明或定义，可参照内核具体实现以及业界C库标准定义在`//kernel/liteos_m/kal/libc/newlib/porting/include`中进行补充